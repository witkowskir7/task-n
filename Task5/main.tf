data "aws_vpc" "selected" {
  default = true
}

resource "aws_default_subnet" "default_az1" {
  availability_zone = "eu-west-1a"
}

module "jenkins" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name           = "ec2"
  instance_count = 1

  #ami                    = "ami-02b4e72b17337d6c1"   #main
  ami                    = "ami-0df20d080185f1fa0"   #jenkins&docker
  instance_type          = "t3a.micro"
  key_name               = "key-1"
  monitoring             = false
  vpc_security_group_ids = [aws_security_group.jenkins.id]
  subnet_id              = aws_default_subnet.default_az1.id
  iam_instance_profile = aws_iam_instance_profile.jenkins_profile.id

  user_data = data.template_file.init.rendered

}

resource "aws_security_group" "jenkins" {
  name        = "jenkins_securityG"
  description = "jenkins security group"
  vpc_id      = data.aws_vpc.selected.id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }

  ingress {
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}



data "template_file" "init" {
  template = file("${path.module}/jenkins.sh.tpl")
}
###############----------------------


resource "aws_iam_role" "ec2_role" {
  name = "ec2"
  assume_role_policy = jsonencode({
    Version : "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "ec2_ssm_role" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role_policy_attachment" "ec2_s3_role" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "policy_attachment" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_instance_profile" "jenkins_profile" {
  name = "jenkins_role"
  role = aws_iam_role.ec2_role.name
}