#!/bin/bash
sudo yum update -y
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform git
amazon-linux-extras install ansible2 -y
amazon-linux-extras install epel -y
amazon-linux-extras install docker
sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
usermod -a -G docker jenkins
sudo yum upgrade
sudo yum install jenkins java-1.8.0-openjdk-devel -y
sudo systemctl daemon-reload
sudo systemctl start jenkins
chkconfig jenkins on
sudo systemctl start docker && sudo systemctl enable docker && sudo systemctl status docker
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
