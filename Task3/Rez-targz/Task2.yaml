Parameters:
  KeyPair: 
    Description: Amazon EC2 Key Pair
    Default: "key-1"
    Type: "AWS::EC2::KeyPair::KeyName"
 
  InstanceTypeParameter:
    Type: String
    Default: t3.micro
    AllowedValues:
      - t3a.micro
      - t3.micro
      - t3.small
    Description: Enter t3a.micro, m1.small, or m1.large. Default is t2.micro.

  DefCidr: 
    Description: 0
    Default: 0.0.0.0/0
    Type: String

  DBTypeParameter:
    Type: String
    Default: db.t3.micro
    AllowedValues:
      - db.t3.micro
      - db.t3.small
      - db.t3.medium 

  DBInstanceID:
    Default: mydbinstance
    Description: My database instance
    Type: String
    MinLength: '1'
    MaxLength: '63'
    AllowedPattern: '[a-zA-Z][a-zA-Z0-9]*'

  DBName:
    Default: wordpressdb
    Description: The WordPress database name
    Type: String
  DBUsername:
    NoEcho: 'true'
    Type: String
    Default: adminname
  DBPassword:
    NoEcho: 'true'
    Type: String
    Default: adminpass123
  DBAllocatedStorage:
    Default: '5'
    Description: The size of the database (Gb)
    Type: Number

  MultiAZDatabase:
    Default: 'false'
    Type: String
    AllowedValues:
      - 'true'
      - 'false'

  WebServerCapacity:
    Default: '2'
    Description: The initial number of Webservers instances
    Type: Number
    MinValue: '1'
    MaxValue: '5'
    ConstraintDescription: must be between 1 and 5 EC2 instances

####
Mappings:
  RegionMap:
    eu-west-1:
     AMI: ami-07b0aa980abdf8caa

####===========
Resources: 
  LaunchConfig: 
    Type: AWS::AutoScaling::LaunchConfiguration
    Metadata:
      'AWS::CloudFormation::Init':
        configSets:
          wordpress_install:
            -install_wordpress
        install_wordpress:
          files: 
            /tmp/create-wp-config:
              content: !Join
                - ''
                - - |
                    #!/bin/bash
                  - >
                    cp /var/www/html/wordpress/wp-config-sample.php
                    /var/www/html/wordpress/wp-config.php
                  - sed -i "s/'database_name_here'/'
                  - !Ref DBName
                  - |
                    '/g" wp-config.php
                  - sed -i "s/'username_here'/'
                  - !Ref DBUsername
                  - |
                    '/g" wp-config.php
                  - sed -i "s/'password_here'/'
                  - !Ref DBPassword
                  - |
                    '/g" wp-config.php
                  - sed -i "s/'localhost'/'
                  - !GetAtt
                    - DBInstance
                    - Endpoint.Address
                  - |
                    '/g" wp-config.php
              mode: '000500'
              owner: root
              group: root
          commands:
            01_configure_wordpress:
              command: /tmp/create-wp-config
              cwd: /var/www/html/wordpress
          services:
            sysvinit:
              httpd:
                enabled: 'true'
                ensureRunning: 'true'
    Properties:
      SecurityGroups: 
        - !Ref EC2SecurityGroup
      ImageId: !FindInMap 
        - RegionMap
        - !Ref 'AWS::Region'
        - AMI
      KeyName: "key-1"
      InstanceType: !Ref InstanceTypeParameter
      UserData:
        Fn::Base64: !Sub
        - |
            #!/bin/bash
  
            cd /var/www/html
            mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 ${MountTarget1.IpAddress}:/ /var/www/html/wp-content
            sed -i "s/'database_name_here'/'${DBName}'/g" wp-config.php
            sed -i "s/'username_here'/'${DBUsername}'/g" wp-config.php
            sed -i "s/'password_here'/'${DBPassword}'/g" wp-config.php       
            sed -i "s/'localhost'/'${Task2RDSHost}'/g" wp-config.php  

            systemctl start httpd
        - Task2RDSHost : !GetAtt DBInstance.Endpoint.Address

     
  ASG:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      MinSize: '1'
      MaxSize: '5'
      LaunchConfigurationName: !Ref LaunchConfig
      VPCZoneIdentifier:
        - !ImportValue task1-PrivateSubnetA
        - !ImportValue task1-PrivateSubnetB
      TargetGroupARNs: 
        - !Ref ALBTargetGroup
      DesiredCapacity: !Ref WebServerCapacity
    UpdatePolicy:
      AutoScalingRollingUpdate:
        MinInstancesInService: '1'
        MaxBatchSize: '2'

####------ RDS
  DBInstance:
    Type: 'AWS::RDS::DBInstance'
    Properties:
      DBInstanceIdentifier: !Ref DBInstanceID
      DBName: !Ref DBName 
      Engine: MySQL
      EngineVersion: 8.0.16
      DBInstanceClass: !Ref DBTypeParameter
      MultiAZ: !Ref MultiAZDatabase
      AllocatedStorage: !Ref DBAllocatedStorage
      MasterUsername: !Ref DBUsername
      MasterUserPassword: !Ref DBPassword
      DBSubnetGroupName: !Ref DBSubnetGroup
      VPCSecurityGroups:
        - !GetAtt
          - DBEC2SecurityGroup
          - GroupId
        
####-------- Subnet

  DBSubnetGroup:
    Type: AWS::RDS::DBSubnetGroup
    Properties:
      DBSubnetGroupDescription: description
      SubnetIds:
      - !ImportValue task1-DatabaseSubnetA
      - !ImportValue task1-DatabaseSubnetB

################# EFS
 
  FileSystemResource:
    Type: 'AWS::EFS::FileSystem'
    Properties:
      Encrypted: false
      FileSystemTags:
        - Key: Name
          Value: TestFileSystem
 
  MountTarget1: 
    Type: AWS::EFS::MountTarget
    Properties: 
      FileSystemId: !Ref FileSystemResource
      SubnetId: !ImportValue task1-PrivateSubnetA
      SecurityGroups:
        - Ref: InternalEC2SG
 
  MountTarget2: 
    Type: AWS::EFS::MountTarget
    Properties: 
      FileSystemId: !Ref FileSystemResource
      SubnetId: !ImportValue task1-PrivateSubnetB
      SecurityGroups:
        - Ref: InternalEC2SG

  AccessPointResource:
    Type: 'AWS::EFS::AccessPoint'
    Properties:
      FileSystemId: !Ref FileSystemResource
      PosixUser:
        Uid: "48"
        Gid: "48"
        SecondaryGids:
          - "48"
          - "48"
      RootDirectory:
        CreationInfo:
          OwnerGid: "708798"
          OwnerUid: "7987987"
          Permissions: "0755"
        Path: "/wp-content"

####------- SecurityGroup
  InternalEC2SG:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: InternalEC2SG
      VpcId: !ImportValue task1-VPC
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '2049'
          ToPort: '2049'
          SourceSecurityGroupId: !Ref EC2SecurityGroup

  EC2SecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:     
      GroupDescription: EC2SecurityGroup   
      VpcId: !ImportValue task1-VPC
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '80'
          ToPort: '80'
          SourceSecurityGroupId: !Ref ApplicationLoadBalancerSecurityGroup
        - IpProtocol: tcp
          FromPort: '22'
          ToPort: '22'
          CidrIp: !Ref DefCidr
      

  DBEC2SecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: Open database for access
      VpcId: !ImportValue task1-VPC
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '3306'
          ToPort: '3306'
          SourceSecurityGroupId: !Ref EC2SecurityGroup
      
  ApplicationLoadBalancerSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: Enable 80
      VpcId: !ImportValue task1-VPC
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '80'
          ToPort: '80'
          CidrIp: !Ref DefCidr

####--------- ALB

  ApplicationLoadBalancer:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties: 
      SecurityGroups: 
       - !Ref ApplicationLoadBalancerSecurityGroup  
      Subnets: 
       - !ImportValue task1-PublicSubnetA
       - !ImportValue task1-PublicSubnetB


####  Listener port 80
  ALBlistener:
    Type: "AWS::ElasticLoadBalancingV2::Listener"
    Properties:
      DefaultActions:
        - Type: forward
          TargetGroupArn: !Ref ALBTargetGroup
      LoadBalancerArn: !Ref ApplicationLoadBalancer
      Port: 80
      Protocol: "HTTP"

#### Targer Group
  ALBTargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:   
      HealthCheckPath: /wordpress/wp-admin/install.php
      HealthCheckIntervalSeconds: 10
      HealthCheckTimeoutSeconds: 5
      HealthyThresholdCount: 2     
      Name: String
      Port: 80
      Protocol: HTTP
      Matcher: 
        HttpCode: 200,302    
      UnhealthyThresholdCount: 5
      VpcId: !ImportValue task1-VPC



####-------- Outputs
Outputs:
  WebsiteURL:
    Value: !Join
      - ''
      - - 'http://'
        - !GetAtt
          - ApplicationLoadBalancer
          - DNSName
    Description: WordPress Website
