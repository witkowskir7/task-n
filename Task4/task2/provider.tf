terraform {
  backend "s3" {
    bucket = "buckets3n1"
    key    = "network/task2.tfstate"
    region = "eu-west-1"
  }
}

provider "aws" {
  region = "eu-west-1"
}