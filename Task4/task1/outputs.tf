output "vpc" {
  description = "myVPC"
  value       = module.vpc
}

output "EC2Instance_ip" {
  description = "EC2Instance ip address"
  value       = module.ec2_cluster.public_ip
}

output "ssh_security_group" {
  description = "EC2Instance security group"
  value       = module.ssh_security_group
}