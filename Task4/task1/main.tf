module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.2.0"

  name = "myVPC-1"
  cidr = "10.0.0.0/16"

  azs              = ["eu-west-1a", "eu-west-1b"]
  public_subnets   = ["10.0.1.0/24", "10.0.2.0/24"]
  private_subnets  = ["10.0.10.0/24", "10.0.11.0/24"]
  database_subnets = ["10.0.20.0/24", "10.0.21.0/24"]

  #enable_nat_gateway   = true
  #single_nat_gateway   = true
  #enable_dns_hostnames = true
  #enable_dns_support   = true
  #create_igw           = true

  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false
  enable_vpn_gateway     = false


  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

module "ssh_security_group" {
 
  source  = "terraform-aws-modules/security-group/aws//modules/ssh"
  description = "Security group for SSH"
 
  version = "~> 4.3.0"
  name   = "BastionSG"
  vpc_id = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["ssh-tcp"]
  egress_rules        = ["all-all"]

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

module "ec2_cluster" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name           = "EC2instance"
  instance_count = 1

  ami                    = "ami-0f89681a05a3a9de7"
  instance_type          = "t3.micro"
  key_name               = "key-1"
  monitoring             = false
  vpc_security_group_ids = [module.ssh_security_group.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}






