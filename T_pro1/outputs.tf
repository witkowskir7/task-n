output "infra" {
  value = {
    www_host = local.env.Properties.hosted_zone_name
    alb_host = module.alb.lb_dns_name
    cloudfront_id = aws_cloudfront_distribution.alb_distribution.id
    iam_cf_key = aws_iam_access_key.cf.id
#    iam_cf_secret = aws_iam_access_key.cf.secret
  }
  sensitive = false
}
