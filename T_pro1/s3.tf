resource "aws_s3_bucket" "backup" {
  bucket = "a-${local.tags.environment}-s3-${local.tags.application}-backup"
  acl    = "private"

  lifecycle_rule {
    enabled = true

    expiration {
      days = 30
    }
  }
}

resource "aws_iam_role_policy_attachment" "ec2" {
  role       =  aws_iam_role.ssm.name
  policy_arn = aws_iam_policy.ec2.arn
}

resource "aws_iam_policy" "ec2" {
  name = "a-${local.tags.environment}-iam-${local.tags.application}-s3-access"
 
  policy = jsonencode(
{
  "Version": "2012-10-17"
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.backup.arn}",
        "${aws_s3_bucket.backup.arn}/*"
      ]
    }
  ]
}
  )
}


