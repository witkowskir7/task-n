locals {
  data     = yamldecode(file("data.yaml"))
  defaults = yamldecode(file("defaults.yaml"))
  dynamic_tags = {
    environment = terraform.workspace
  }
  env = merge(
    local.defaults,
    yamldecode(file("${terraform.workspace}/env.yaml"))
  )
  rds = merge(
    local.defaults,
    yamldecode(file("${terraform.workspace}/rds.yaml"))
  )
  tags = merge(local.env.tags, local.dynamic_tags, local.defaults.tags)
}

