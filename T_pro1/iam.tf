resource "aws_iam_access_key" "cf" {
  user    = aws_iam_user.cf.name
}
resource "aws_iam_user" "cf" {
  name = "a-${local.tags.environment}-iam-${local.tags.application}-cf"
}
         
resource "aws_iam_user_policy" "cf" {
  name = "a-${local.tags.environment}-iam-${local.tags.application}-cf"
  user = aws_iam_user.cf.name
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Action": [
            "cloudfront:CreateInvalidation",
            "cloudfront:GetDistribution",
            "cloudfront:GetStreamingDistribution",
            "cloudfront:GetDistributionConfig",
            "cloudfront:GetInvalidation",
            "cloudfront:ListInvalidations",
            "cloudfront:ListStreamingDistributions",
            "cloudfront:ListDistributions"
      ],
      "Effect": "Allow",
      "Resource": "*"   
    }  
  ]
}
EOF
}


    
