module "acm" {
  source                    = "terraform-aws-modules/acm/aws"
  version                   = "~> 3.0"
  domain_name               = local.env.Properties.hosted_zone_name
  zone_id                   = local.env.Properties.hosted_zone_id
  subject_alternative_names = ["www.${local.env.Properties.hosted_zone_name}"]
  wait_for_validation       = true
  tags                      = local.tags
}

module "asg" {
  source                    = "terraform-aws-modules/autoscaling/aws"
  version                   = "~> 4.4.0"
  name                      = "a-${local.tags.environment}-asg-${local.tags.application}"
  image_id                  = local.defaults.Instance.ami
  instance_type             = local.defaults.Instance.type
  spot_price                = local.defaults.Instance.price
  security_groups           = [aws_security_group.webserver.id]
  health_check_type         = "EC2"
  health_check_grace_period = 200
  target_group_arns         = module.alb.target_group_arns
  force_delete              = true
  tags_as_map               = local.tags
  iam_instance_profile_name = "a-${local.tags.environment}-iam-${local.tags.application}-profile"
  lc_name                   = "a-${local.tags.environment}-lc-${local.tags.application}"
  use_lc                    = true
  create_lc                 = true
  update_default_version    = true
  vpc_zone_identifier       = data.terraform_remote_state.network.outputs.vpc.private_subnets
  min_size                  = 1
  max_size                  = 2
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  key_name                  = local.defaults.Instance.key

  user_data = <<EOF
#!/bin/bash
echo "
export WP_DB_HOST=${module.rds.db_instance_endpoint}
export WP_DB_USER=${module.rds.db_instance_username}
export WP_DB_PASS=${module.rds.db_master_password}
export WP_DB_NAME=${module.rds.db_instance_name}
" > /etc/profile.d/wp_env.sh

echo "
#!/bin/bash
DATE=\`date +'%Y-%m-%d_%H:%M:%S'\`
CMD=\"/tmp/backup\"
PARAM=\"--exclude='*/cache/*'\"

mkdir -p \$CMD
cd /var/www/html

wp db export wp-database.sql
tar $PARAM -czf \$CMD/wp-files-\$DATE.tar.gz .
rm -rf wp-database.sql

aws s3 cp \$CMD/wp-files-\$DATE.tar.gz s3://a-${local.tags.environment}-s3-${local.tags.application}-backup

rm -rf \$CMD/wp-files-\$DATE.tar.gz
" > /etc/cron.daily/0backup.sh
chmod +x /etc/cron.daily/0backup.sh

echo "
#!/bin/bash
cd /var/www/html/
/usr/bin/wp core download --skip-content --version=5.8.1
/usr/bin/wp config create --dbname=\$WP_DB_NAME --dbuser=\$WP_DB_USER --dbpass=\$WP_DB_PASS --dbhost=\$WP_DB_PASS --dbprefix='tsh_' --skip-check --skip-plugins --skip-themes
 define('WP_AUTO_UPDATE_CORE', false );" >> /var/www/html/wp-config.php
" > /root/install_wp.sh

### END SCRIPTS ###

### create swap ###
dd if=/dev/zero of=/swapfile bs=1024 count=120000
mkswap /swapfile && chmod 600 /swapfile && swapon /swapfile

### SSM ###
mkdir /tmp/ssm
curl https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm -o /tmp/ssm/amazon-ssm-agent.rpm
sudo yum install -y /tmp/ssm/amazon-ssm-agent.rpm
sudo systemctl start amazon-ssm-agent
### SSM END ###

yum -q -y install yum-utils bind-utils nfs-utils wget rsync
yum -q -y install httpd mod_ssl
amazon-linux-extras install mariadb10.5
amazon-linux-extras enable php7.4 
yum -q -y install yum install php php-mcrypt php-mysqlnd php-mbstring php-fpm php-gd php-xml php-pdo php-pecl-crypto php-opcache
sed -i 's/AllowOverride None/AllowOverride All/ig' /etc/httpd/conf/httpd.conf
systemctl is-enabled httpd          
systemctl enable httpd
ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime

mac=$(curl -s http://169.254.169.254/latest/meta-data/network/interfaces/macs/| head -n1 | tr -d '/')
cidr=$(curl -s http://169.254.169.254/latest/meta-data/network/interfaces/macs/$mac/vpc-ipv4-cidr-block|cut -d'.' -f1-3)
efs_ip=$(host -t A ${aws_efs_file_system.roger.id}.efs.${local.data[terraform.workspace].aws.region}.amazonaws.com $cidr.2|grep "has address"|awk '{print $4}')
umask 0022

chown apache:apache /var/www/
mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 $efs_ip:/ /var/www/
mkdir -p /var/www/html/
echo "1" > /var/www/html/test.php

### INSTALL WORDPRESS ###
curl -o /tmp/wp-cli.phar https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x /tmp/wp-cli.phar
mv /tmp/wp-cli.phar /usr/bin/wp

systemctl start httpd

echo "
CloudFront_ID:   ${aws_cloudfront_distribution.alb_distribution.id}
AccessKey:       ${aws_iam_access_key.cf.id}
SecretAccessKey: ${aws_iam_access_key.cf.secret}
RDS_Pass:        ${module.rds.db_master_password}
" > /root/wp_infra_info

EOF

  root_block_device = [
    {
      volume_size = "20"
      volume_type = "gp2"
      encrypted   = true
      delete_on_termination = true
    },
  ]

  instance_market_options = {
    market_type  = "spot"
    spot_options = {
      block_duration_minutes = 60        
    }
  }

  depends_on = [
    module.rds
  ]
}

module "alb" {
  source             = "terraform-aws-modules/alb/aws"
  version            = "~> 6.2.0"
  name               = "a-${local.tags.environment}-alb-${local.tags.application}"
  subnets            = data.terraform_remote_state.network.outputs.vpc.public_subnets
  vpc_id             = data.terraform_remote_state.network.outputs.vpc.vpc_id
  load_balancer_type = "application"
  security_groups    = [aws_security_group.albsg.id]

  target_groups = [
    {
      name             = "a-${local.tags.environment}-tg-${local.tags.application}-ssl"
      backend_protocol = "HTTPS"
      backend_port     = 443
      target_type      = "instance"
      health_check = {
        healthy_threshold   = 3
        protocol            = "HTTPS"
        port                = 443
        interval            = 10
        path                = "/test.php"
        timeout             = 6
        unhealthy_threshold = 5
        matcher             = "200"
      }
    },
    {
      name             = "a-${local.tags.environment}-tg-${local.tags.application}"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
      health_check = {
        healthy_threshold   = 3
        interval            = 10
        path                = "/test.php"
        timeout             = 6
        unhealthy_threshold = 5
        matcher             = "200"
      }
    }
  ]
  https_listeners = [
    {
      port               = 443
      protocol           = "HTTPS"
      certificate_arn    = module.acm.acm_certificate_arn
      target_group_index = 0
    }
  ]
  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 1
    }
  ]
  tags = local.tags
}

resource "aws_security_group" "albsg" {
  name        = "a-${local.tags.environment}-sg-${local.tags.application}-alb"
  description = "Alb security group"
  vpc_id      = data.terraform_remote_state.network.outputs.vpc.vpc_id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description = "http config"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "https config"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "webserver" {
  name        = "a-${local.tags.environment}-sg-${local.tags.application}-ec2"
  description = "Security group for application load balancer"
  vpc_id      = data.terraform_remote_state.network.outputs.vpc.vpc_id

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.albsg.id]
  }
  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    security_groups = [aws_security_group.albsg.id]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_route53_record" "alb" {
  zone_id = local.env.Properties.hosted_zone_id
  name    = local.env.Properties.hosted_zone_name
  type    = "A"

  alias {
    name = aws_cloudfront_distribution.alb_distribution.domain_name
    zone_id = aws_cloudfront_distribution.alb_distribution.hosted_zone_id
    evaluate_target_health = false
  }

#  alias {
#    name                   = module.alb.lb_dns_name
#    zone_id                = module.alb.lb_zone_id
#    evaluate_target_health = true
#  }
}

resource "aws_route53_record" "alb_aaaa" {
  zone_id = local.env.Properties.hosted_zone_id
  name    = local.env.Properties.hosted_zone_name
  type    = "AAAA"

  alias {
    name = aws_cloudfront_distribution.alb_distribution.domain_name
    zone_id = aws_cloudfront_distribution.alb_distribution.hosted_zone_id
    evaluate_target_health = false
  }

#  alias {
#    name                   = module.alb.lb_dns_name
#    zone_id                = module.alb.lb_zone_id
#    evaluate_target_health = true
#  }
}

resource "aws_route53_record" "www" {
  zone_id  = local.env.Properties.hosted_zone_id
  name     = "www"
  type     = "CNAME"
  ttl      = "60"
  records  = ["${local.env.Properties.hosted_zone_name}"]
}

resource "aws_efs_file_system" "roger" {
  creation_token = "efs-rosourse"
  tags           = local.tags
}

resource "aws_efs_mount_target" "roger" {
  count           = length(data.terraform_remote_state.network.outputs.vpc.private_subnets)
  file_system_id  = aws_efs_file_system.roger.id
  subnet_id       = data.terraform_remote_state.network.outputs.vpc.private_subnets[count.index]
  security_groups = [aws_security_group.efs.id]
}

resource "aws_security_group" "efs" {
  name        = "a-${local.tags.environment}-efs-${local.tags.application}"  
  description = "efs security group"
  vpc_id      = data.terraform_remote_state.network.outputs.vpc.vpc_id  

  ingress {
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
    security_groups = [aws_security_group.webserver.id]
  }
}

module "rds" {
  source  = "terraform-aws-modules/rds/aws"
  identifier                = "a-${local.tags.environment}-rds-${local.tags.application}"
  engine                    = local.rds.Instance.engine
  instance_class            = local.rds.Instance.type
  allocated_storage         = 10
  name                      = "roger_${local.tags.environment}"
  username                  = "roger_rds_user"
  create_random_password    = true
  create_db_option_group    = false
  create_db_parameter_group = false
  multi_az                  = false
  skip_final_snapshot       = true
  subnet_ids                = data.terraform_remote_state.network.outputs.vpc.database_subnets
  vpc_security_group_ids    = [aws_security_group.rds.id]
}

resource "aws_security_group" "rds" {
  name   = "a-${local.tags.environment}-sg-${local.tags.application}-rds" 
  vpc_id = data.terraform_remote_state.network.outputs.vpc.vpc_id 
}

resource "aws_security_group_rule" "rds_webserver" {
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.webserver.id
  security_group_id        = aws_security_group.rds.id
}

 
