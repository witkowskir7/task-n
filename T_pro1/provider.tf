provider "aws" {
  region  = "eu-west-1"
  profile = terraform.workspace
}

provider "aws" {
  region  = "us-east-1"
  alias   = "us-east-1"
  profile = terraform.workspace
}

