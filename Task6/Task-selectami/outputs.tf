output "lb_dns" {
  description = "lb dns address"
  value       = module.alb.lb_dns_name
}