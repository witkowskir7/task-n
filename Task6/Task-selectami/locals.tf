locals {
  userdata = <<-USERDATA
#!/bin/bash
cd /var/www/html
cp /var/www/html/wp-config-sample.php /var/www/html/wp-config.php
mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 ${aws_efs_file_system.default.dns_name}:/ /var/www/html/wp-content
sed -i "s/'database_name_here'/'wordpress'/g" wp-config.php
sed -i "s/'username_here'/'adminname'/g" wp-config.php
sed -i "s/'password_here'/'adminpass123'/g" wp-config.php       
sed -i "s/'localhost'/'${module.rds.db_instance_address}'/g" wp-config.php
wp theme install twentytwentyone --activate
sudo chown -R apache:apache /var/www/html/wp-content
service httpd start
  USERDATA
}