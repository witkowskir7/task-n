data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = "buckets3n1"
    key    = "network/task1.tfstate"
    region = "eu-west-1"
  }
}


#####Webserver

module "asg" {
  source                    = "terraform-aws-modules/autoscaling/aws"
  version                   = "~> 4.4.0"
  name                      = "webserverGroup"
  lc_name                   = "launchconfig"
  image_id                  = "ami-0f89681a05a3a9de7"
  instance_type             = "t3a.micro"
  security_groups           = [aws_security_group.webserver.id]
  health_check_grace_period = 200
  target_group_arns     = module.alb.target_group_arns
  force_delete = true

  #---#---# Auto scaling group
  lt_name                   = "newwordpress"
  use_lt                    = true
  create_lt                 = true
  update_default_version    = true
  vpc_zone_identifier       = data.terraform_remote_state.vpc.outputs.vpc.private_subnets
  health_check_type         = "ELB"
  min_size                  = 2
  max_size                  = 5
  desired_capacity          = 2
  wait_for_capacity_timeout = 0
  key_name                  = "key-1"
  user_data_base64          = base64encode(local.userdata)
  
}

####ALB

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 6.2.0"

  name = "my-alb"

  load_balancer_type = "application"

  vpc_id          = data.terraform_remote_state.vpc.outputs.vpc.vpc_id
  subnets         = data.terraform_remote_state.vpc.outputs.vpc.public_subnets
  security_groups = [aws_security_group.albsg.id]
  enable_http2    = false


  target_groups = [
    {
      name_prefix      = "albtg"
      backend_protocol = "HTTP"
      backend_port     = 80
      health_check = {
        healthy_threshold   = 3
        interval            = 10
        path                = "/wp-admin/install.php"
        port                = 80
        timeout             = 6
        unhealthy_threshold = 5
        matcher             = "200,302"
      }

    }
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]

}

resource "aws_security_group" "albsg" {
  name        = "alb_securityG"
  description = "Alb security group"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc.vpc_id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description = "http config"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


####DB

module "rds" {
  source  = "terraform-aws-modules/rds/aws"
  identifier                = "wordpressdb"
 

  engine                    = "mysql"
  instance_class            = "db.t3.micro"
  allocated_storage         = 10
  name                      = "wordpress" 

  username                  = "adminname"
  password                  = "adminpass123"
  
  create_random_password    = false
  create_db_option_group    = false
  create_db_parameter_group = false
  multi_az                  = false

  skip_final_snapshot       = true

  subnet_ids             = data.terraform_remote_state.vpc.outputs.vpc.database_subnets
  vpc_security_group_ids = [aws_security_group.rds.id]
}

resource "aws_security_group" "rds" {
  name   = "rds_securityG"
  vpc_id = data.terraform_remote_state.vpc.outputs.vpc.vpc_id
}
 

resource "aws_security_group_rule" "rds_webserver" {
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.webserver.id
  security_group_id        = aws_security_group.rds.id
}


#### EFS

resource "aws_efs_file_system" "default" {
  creation_token = "efs-rosourse"
}

resource "aws_efs_mount_target" "default" {
  count = length(data.terraform_remote_state.vpc.outputs.vpc.private_subnets)
  file_system_id  = aws_efs_file_system.default.id
  subnet_id       = data.terraform_remote_state.vpc.outputs.vpc.private_subnets[count.index]
  security_groups = [aws_security_group.efs.id]
}

resource "aws_security_group" "efs" {
  name        = "efs_securityG"
  description = "efs security group"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc.vpc_id

  ingress {
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
    security_groups = [aws_security_group.webserver.id]
  }
}

#### EC2Instance SG

resource "aws_security_group" "webserver" {
  name        = "webserver_security"
  description = "Security group for application load balancer"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc.vpc_id

##--- Allow output traffic


  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.albsg.id]
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [data.terraform_remote_state.vpc.outputs.ssh_security_group.security_group_id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}